# Review Tracker Code Challenge

## Installation instructions
### Prerequisites
- Ruby v2.3.1
- a postgres DB named `rtchallenge`
- node v8+ (if you want to rebuild the JavaScript bundle, latest version already in the repo)

### Install
Perform the database migration:
`rake db:migrate`

Seed the database:
`rake db:seed`

Install all the ruby dependencies:
`bundle install`

Build the React JavaScript bundle (optional):
```
npm install
npm run build
```
This will output a bundle.js file in the `public` directory which will be served up when the Sinatra server is started.

Start up the Sinatra server:
`source env.sh && bundle exec rackup`
This will run the Sinatra server on the default port (9292), so the project should be available at `http://localhost:9292/`

### Notes
I did end up altering the back-end slightly, though I tried my hardest to leave it as similar as was given to me.  Most notably I made a seed file to seed the DB instead of having to run the update sprint command.  I also moved the routes in the `rtchallenge.rb` file from their previous location to instead be prefixed with `/api` so that I could serve the react project from the root.  I also added a protected POST `/auth` route which purely checks the auth header and sends back a 200 if successful.  Lastly, I changed the `RELEASE_LABEL` env variable to be `jason_submission` (otherwise the update sprint route would crash) and created a shell file to export all the variables.

I made the frontend using react, webpack, and material-ui next.  I used a theme wrapper and used the color scheme I saw on the review trackers website.  As far as features goes, I put in: 

- Basic authentication using state to set the auth header on all requests to the Sinatra server.
- Responsiveness for mobile using the material UI grid
- Loading state indicator while retrieving the story data from the Sinatra server
- Separating of each of the stories into columns by their status
- A Hamburger menu in the top left to either visit the pivotal tracker site, hit the update_sprint route on the sinatra server, or log out
- A seach box at the top to fuzzy search filter story cards by values in either the name field or tag field