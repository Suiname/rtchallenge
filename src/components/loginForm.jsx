import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { FormControl } from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

const styles = theme => ({
  loginContainer: {
    display: 'flex',
    margin: '0 auto',
    height: 250,
    width: 350,
    marginTop: '15%',
    border: `2px solid ${theme.palette.primary[500]}`,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  button: {
    color: theme.palette.secondary[50],
    marginTop: 20,
  },
  formControl: {
    width: 200,
    height: 250,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
});

const loginForm = (props) => {
  const { classes, name, password, handleChange, onSubmit } = props;
  return (
    <div className={classes.loginContainer}>
      <FormControl className={classes.formControl}>
        <TextField
          id="username"
          label="username"
          className={classes.textField}
          value={name}
          onChange={handleChange('username')}
          margin="normal"
          />
        <TextField
          id="password"
          label="password"
          className={classes.textField}
          type="password"
          value={password}
          onChange={handleChange('password')}
          margin="normal"
          />
        <Button raised color="primary" className={classes.button} onClick={onSubmit}>
          Log In
        </Button>
      </FormControl>
    </div>
  )
};

loginForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(loginForm);