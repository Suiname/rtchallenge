import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import Menu, { MenuItem } from 'material-ui/Menu';

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 3,
    width: '100%',
  },
  flex: {
    flex: 1,
    color: theme.palette.secondary[50],
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    color: theme.palette.secondary[50],
  },
});

function appBar(props) {
  const { classes, anchorEl, helpers } = props;
  const { handleClick, handleClose, visitPT, updateSprint, logOut } = helpers;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton className={classes.menuButton} onClick={handleClick} aria-label="Menu">
            <MenuIcon />
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={visitPT}>Visit Pivotal Tracker</MenuItem>
            <MenuItem onClick={updateSprint}>Update Sprint</MenuItem>
            <MenuItem onClick={logOut}>Logout</MenuItem>
          </Menu>
          <Typography type="title" className={classes.flex}>
            Review Tracker Challenge
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

appBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(appBar);