import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import red from 'material-ui/colors/red';
import Chip from 'material-ui/Chip';

const styles = theme => ({
  card: {
    margin: 10,
  },
  media: {
    height: 194,
  },
  avatar: {
    backgroundColor: theme.palette.primary[500],
  },
  chip: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.primary[500],
    color: theme.palette.secondary[50],
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  type: {
    textAlign: 'left',
    fontFamily: theme.typography.fontFamily,
  },
  state: {
    textAlign: 'right',
    fontFamily: theme.typography.fontFamily,
    marginLeft: 'auto',
  }
});

const storyCard = (props) => {
  const { classes, story } = props;
  return (
      <Card key={story.id} className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="Estimate" className={classes.avatar}>
              {story.estimate}
            </Avatar>
          }
          title={story.name}
          subheader={new Date(story.created_at).toLocaleDateString()}
        />
        <CardContent>
          <div className={classes.row}>
            {story.labels.map((label, i) => <Chip key={i} label={label.name} className={classes.chip} />)}
          </div>
        </CardContent>
        <CardActions>
          <span className={classes.type}>
            Story type: {story.story_type}
          </span>
          <span className={classes.state}>
            State: {story.current_state}
          </span>
        </CardActions>
      </Card>
  );
};

export default withStyles(styles)(storyCard);