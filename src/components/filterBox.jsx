import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon'
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';

const styles = theme => ({
  filterContainer: {
    display: 'flex',
    margin: '0 auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  button: {
    color: theme.palette.secondary[50],
    marginTop: 20,
  },
  formControl: {
    height: 50,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
});

const filterBox = (props) => {
  const { classes, filter, handleChange } = props;
  return (
    <div className={classes.filterContainer}>
      <FormControl className={classes.formControl}>
      <InputLabel >Filter</InputLabel>
      <Input
        id="adornment-password"
        type="text"
        value={filter}
        onChange={handleChange('filter')}
        startAdornment={
          <InputAdornment position="start">
            <Icon>search</Icon>
          </InputAdornment>
        }
      />
    </FormControl>
    </div>
  )
};

filterBox.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(filterBox);