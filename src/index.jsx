import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import Root from './container/Root';
import red from 'material-ui/colors/red';
import lightBlue from 'material-ui/colors/lightBlue';
import grey from 'material-ui/colors/grey'

const theme = createMuiTheme({
  palette: {
    primary: {
      ...lightBlue,
      '500': '#00add7',
    },
    secondary: grey,
    error: red,
  },
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Root />
  </MuiThemeProvider>,
  document.getElementById('root')
);