import React from 'react';
import fetch from 'isomorphic-fetch';
import header from 'basic-auth-header';
import Fuse from 'fuse.js';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import { CircularProgress } from 'material-ui/Progress';
import Snackbar from 'material-ui/Snackbar';

import AppBar from '../components/appBar';
import LoginForm from '../components/loginForm';
import StoryCard from '../components/storyCard';
import FilterBox from '../components/filterBox';


const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: 30,
  },
  storyHeader: theme.mixins.gutters({
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
    textAlign: 'center',
    fontSize: '1.5em',
    backgroundColor: theme.palette.primary[500],
    color: theme.palette.secondary[50],
  }),
  progress: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 50,
    textAlign: 'center',
  },
  snackBar: {
    color: theme.palette.error[500],
  }
});


class Root extends React.Component {
  constructor(props){
    super(props);
    this.state = { loggedIn: false, username:'', password:'', loading: true, stories: [], headers: {}, error: null, filter: '', anchorEl: null };
    this.clearError = this.clearError.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.logOut = this.logOut.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.updateSprint = this.updateSprint.bind(this);
    this.visitPT = this.visitPT.bind(this);
  }
  clearError(){
    this.setState({ error: null });
  }
  componentDidUpdate() {
    const { loggedIn, loading, headers } = this.state;
    if (loggedIn && loading) { // user has logged in but data has not loaded
      fetch('/api/tickets', {
        method: 'GET',
        headers,
      })
      .then((response) => {
        return response.json();
      })
      .then((stories) => {
        this.setState({
          loading: false,
          stories,
        });
      })
      .catch((error) => {
        console.log(error);
        this.setState({ error });
      });
    }
  }
  handleClick(event) {
    this.setState({ anchorEl: event.currentTarget });
  }
  handleChange(name) {
    return (event) => {
      this.setState({
      [name]: event.target.value,
      });
    }
  }
  handleClose() {
    this.setState({ anchorEl: null });
  };
  logOut(){
    this.setState({ loggedIn: false, username:'', password:'', loading: true, stories: [], headers: {}, error: null, filter: '', anchorEl: null });
  }
  onSubmit(){
      const { username, password } = this.state;
      const headers = new Headers();
      headers.append('Authorization', header(username, password));
      fetch('/auth/', {
          method: 'POST',
          headers,
      }).then((response) => {
          if (response.status >= 400) {
            throw new Error("Unauthorized!");
          } else {
            this.setState({ headers, loggedIn: true });
          }
      })
      .catch((error) => {
          this.setState({ error });
      })
  }
  updateSprint(){
    this.handleClose();
    const { headers } = this.state;
    fetch('/api/update_sprint', {
      method: 'GET',
      headers,
    })
    .catch((error) => {
      console.log(error);
      this.setState({ error });
    });
  }
  visitPT(){
    this.handleClose();
    window.open('https://www.pivotaltracker.com/dashboard', '_blank');
  }
  render(){
    const { loggedIn, loading, stories, header, username, password, filter, anchorEl, error } = this.state;
    const { classes } = this.props;
    const AppBarHelpers = {
      handleClick: this.handleClick,
      handleClose: this.handleClose,
      visitPT: this.visitPT,
      updateSprint: this.updateSprint,
      logOut: this.logOut,
    };
    let mainComponent;
    if (!loggedIn) {
      mainComponent = <LoginForm name={username} password={password} handleChange={this.handleChange} onSubmit={this.onSubmit} />;
    }
    else if (loading) {
      mainComponent = (
        <div>
          <AppBar anchorEl={anchorEl} helpers={AppBarHelpers}/>
          <div className={classes.progress}>
            <CircularProgress size={100} />
          </div>
        </div>
      );
    } else { // logged in and data has been fetched
      const options = { // options for Fuse fuzzy searching
        shouldSort: true,
        threshold: 0.3,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: [ // only search through the name and label name fields
          "name",
          "labels.name",
        ],
      };
      const fuse = new Fuse(stories, options);
      const filteredStories = filter ? fuse.search(filter) : stories;
      const inProgress = filteredStories.filter((element) => element.current_state === 'started');
      const finished = filteredStories.filter((element) => element.current_state === 'finished');
      const otherStates = filteredStories.filter((element) => element.current_state != 'finished' && element.current_state != 'started');
      mainComponent = (
        <div>
          <AppBar anchorEl={anchorEl} helpers={AppBarHelpers}/>
          <div className={classes.root}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FilterBox filter={filter} handleChange={this.handleChange} />
              </Grid>
              <Grid item xs={12} lg={4}>
                <div>
                  <Paper className={classes.storyHeader}>In Progress</Paper>
                  {inProgress.map((story) => <StoryCard story={story} key={story.id} />)}
                </div>
              </Grid>
              <Grid item xs={12} lg={4}>
                <div>
                  <Paper className={classes.storyHeader}>Finished</Paper>
                  {finished.map((story) => <StoryCard story={story} key={story.id} />)}
                </div>
              </Grid>
              <Grid item xs={12} lg={4}>
                <div>
                  <Paper className={classes.storyHeader}> Other </Paper>
                  {otherStates.map((story) => <StoryCard story={story} key={story.id} />)}
                </div>
              </Grid>
            </Grid>
          </div>
        </div>
      );
    }

    return (
      <div>
        {mainComponent}
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          open={!!error}
          onClose={this.clearError}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id" className={classes.snackBar}>{error ? error.message : ''}</span>}
        />
      </div>
    );
  }
}

export default withStyles(styles)(Root);