const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: [
    './src/index.jsx',
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  watch: process.env.NODE_ENV === 'development',
  module: {
    rules: [
    {
      test: /\.css$/,
      use: [
        { loader: "style-loader" },
        { loader: "css-loader" }
      ]
    },{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: { 
        loader: 'babel-loader',
        options: {
          presets: ['env', 'react', 'es2015', 'stage-2']
        }
      } 
    }],
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.jsx']
  },
};